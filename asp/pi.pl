pi(V1,V2,V4,V5) :- typeVar(V3), param(V1,V3,V4), param(V2,V3,V5), type(V1,V10,V8), param(V1,V9,V7), typeVar(V9), V7 = 1 .. V8, type(V2,V14,V12), param(V2,V13,V11), typeVar(V13), V11 = 1 .. V12.

notSuper(V1) :- type(V1,_,_), not superWildcard(V1).
notExtends(V1) :- type(V1,_,_), not extendsWildcard(V1).
