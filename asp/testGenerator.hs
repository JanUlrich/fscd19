import Control.Monad.State


type Atom = String
data ASPStatement = Implication [ASPStatement] [Atom] | Cart [SubImplication] | Facts [Atom]
data SubImplication = CartImplication [ASPStatement] [Atom]

data ASPVarState = ASPVarState { tphNum :: Int, other :: [ASPStatement]}

type ASPVar = String

type ASPVarStateMonad  = State ASPVarState
freshASPVar ::   ASPVarStateMonad  ASPVar
freshASPVar  = state  (\st -> let st' = ASPVarState ((tphNum st) +1) (other st) in ("V" ++ (show $ tphNum st'),st') )
freshASPCons ::   ASPVarStateMonad  ASPVar
freshASPCons  = state  (\st -> let st' = ASPVarState ((tphNum st) +1) (other st) in ("c" ++ (show $ tphNum st'),st') )
freshASPFunctionName ::   ASPVarStateMonad  ASPVar
freshASPFunctionName  = state  (\st -> let st' = ASPVarState ((tphNum st) +1) (other st) in ( "fun" ++ (show $ tphNum st'),st') )

data Type = Type String [Type]

testTypes = [Type "Integer" [],
             Type "Comparable" [Type "Integer" []]]

main = putStrLn $ connectAtoms ".\n" $ evalState (createType testTypes) $ ASPVarState 0 []

createType [] = return []
createType (typ:types) = do
    var <- freshASPCons
    add <- createType types
    t <- createTypeWith var typ
    return $ t ++ add

createTypeWith typePointer (Type name paralist) = do
    aspParaList <- createParam typePointer paralist (0 :: Int)
    return $ [(makeASPRule ["type", (typePointer), ("\""++name++"\""), (show $ length paralist)])]
            ++ aspParaList



createParam typePointer [] num = return []

createParam typePointer ((Type name plist):list) num = do
    newType <- freshASPCons
    paramType <- createTypeWith newType (Type name plist)
    add <- createParam typePointer list (num + 1)
    return $ [makeASPRule ["param", typePointer, newType, (show num)]] ++ paramType ++ add

makeASPRule params = (head params) ++ "(" ++ (
    foldl (\ a b -> a ++ "," ++ b) (head . tail $ params) (tail . tail $ params)
    ) ++ ")"

connectAtoms connector atoms = foldl (\ a b -> a ++ connector ++ b) (head atoms) (tail atoms)
    