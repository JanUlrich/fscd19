typeVar(test).
req(test).
req2(test).

{ fun1(req(A), req2(A)) : req(A), req2(A) } == 1 :- typeVar(A).
res(A) :- fun1(req(A), req2(A)), typeVar(A).
res2(A) :- fun1(req(A), req2(A)), typeVar(A).

