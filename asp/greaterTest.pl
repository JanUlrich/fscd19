% erster einfacher TEst
% greater String = Object

typeFC(fct1, "String", 0).
typeFC(fct2, "Object", 0).
smaller(fct1, fct2).

type(test, "String", 0).
greaterGen(test, test2).

% zweiter Test mit Parametern
typeFC(fct3, tVector, 1).
typeFC(fct4, tList, 1).
smaller(fct3, fct4).

paramFC(fct3, tvar1, 1).
paramFC(fct4, tvar1, 1).
typeVar(tvar1).

type(test3, tVector, 1).
param(test3, p1, 1).
type(p1, tString, 0).
greaterGen(test3, test4).


%#show param/3.
#show greaterGen/2.
#show smallerGen/2.
%#show type/3.
%#show pi/4.

