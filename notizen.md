# Anforderungen an Paper
* Es muss klar sein, was das Programm kann
* Es muss klar sien, was das Programm nicht leistet
* In Future Work kann dann noch auf die Komplettierung verwiesen werden
* related work
	* Andere Typinferenzalgorithmen erw�hnen und vergleichen
	* Andere Anwendungen von ASP auf �hnliche Probleme

# Probleme
## Problem: ASP Code komplex
* Die Argumente permutieren die Reihenfolge bei Subtypbeziehungen
	* Hier sollte noch eine Lösung gefunden werden das 1 zu 1 umzusetzen

## Lösungsvorschläge
* Man sollte ihn in Methoden unterteilen
	* Viele Dinge werden mehrfach angewendet
* Man sollte Klassen bauen
	* Wie kann man Constraints zwischen Klassen definieren?

* Den Algorithmus etwas vereinfachen
	* Davon ausgehen, dass bei Subtypen keine Parameter in den Argumenten hinzukommen
	* Problem: Algorithmus übersetzt sich dann nicht mehr 1 zu 1

* einen Precompiler schreiben
	* man könnte ein Konstrukt #newVar(X) einführen
		* Dieses wird dahingehend geändert, dass an dieser Stelle eine eindeutige neue Variable erstellt wird (Typvariable)
	* man könnte den ASP Code programmatisch generieren
		* würde sich für FC anbieten
		* für smallerGen und greaterGen verhält sich der Code sehr ähnlich
		* die sich ändernden stellen könnte ein Programm einfügen
		* oder ein Precompiler (keine Idee wie das gehen soll)

### Haskell für die Generierung des ASP Codes benutzen
* Nach der Form
	* reduce Typ = hier dann der ASP Code der entsteht
* Vorteil hier:
	* Haskell sieht, ob man alle Möglichkeiten der Funktionsaufrufe behandelt hat (es ist typsicher)
	* Man kann redundanzen vermeiden

## Problem: Riesiger Suchbaum wird erzeugt
* Beispiel:
	Matrix <. A
	B <. String
	A <. Vector<B>
	* wird zu:
		A =. Vector<Integer>
		B =. Integer != B <. String
	* erst nach dem Schritt des Karthesischen Produkts (Matrix <. A verwerten) wird der Fehler festgestellt
	* Die Idee war, dass der SAT Solver den meisten Speedup in den Karthesischen Produkten bringt
	* hier scheint es als käme eher der Grounder zum Einsatz
* Der Grounder macht die meiste Arbeit
	* Der Suchbaum ist riesig wenn man alle Möglichkeiten betrachtet
	* Beispielsweise gibt es bei den Reduce-Regeln nie Fälle wie {...} = 1 (hier kommt der SAT-Solver zum Einsatz
	* Es sind meisten einfache Ableitungen A :- b, b2,..
	* Die "isSolvedForm" Regeln sind die Regeln, welche bestimmen ob ein Result Set in solved Form ist
	* Diese sollten so formuliert sein, dass der Grounder schon während der Reduce Regeln Teilbäume ausschließen kann
	* Oder zumindes sehr früh.
	* Worst-Case: Die Solved-Form Filterung erst im Nachhinein durchführen (beispielsweise wieder im Java Programm) -> dann kein Speedup
	* THESE: Regeln sollten so früh wie möglich greifen -> Dann größter Speedup (dies ist nicht überprüft, kann möglicherweise mit Benchmarks belegt werden)
* Idealerweise sollte der Algorithmus folgenermaßen ablaufen:
	* Schon während der Reduce Regeln und der Subtyp-Generierung (Karthesische Produkte) sollte gefiltert werden
* Ihm muss die Möglichkeit gegeben werden schon bei der Erstellung des Suchbaums möglichst viele Teilbäume auszuschliessen.
