import ASPGenerator

main = do
    writeFile "../asp/reduce.pl" $ generate reduce1 ++ generate reduceEq
    return ()

{-
Dem Grounder und Solver soll geholfen werden, indem möglichst früh die Such-Teilbäume abgebrochen werden,
welche zu keinem zulässigen Ergebnis führen

Typ1 <. Typ2 ist falsch
-}
prematureDecline = do    
    t1 <- freshASPVar
    t2 <- freshASPVar
    t1name <- freshASPVar
    t2name <- freshASPVar
    t1param <- freshASPVar
    t2param <- freshASPVar
    req <- return $ [
        SmallerDot t1 t2,
        Type t1 t1name t1param,
        Type t2 t2name t2param,
        -- hier müsste t1name != t2name stehen
        ]
    return $ Implication [] req

