import Control.Monad.State

type Atom = String
data ASPStatement = Implication [ASPStatement] [Atom] | Cart [SubImplication] | Facts [Atom]
data SubImplication = CartImplication [ASPStatement] [Atom]


onlyOne listAnswers req = do
    convertedAnswers <- replaceWithNewFunc listAnswers
    additionalTerms <- return $ zip (map (\(l,r) -> l) listAnswers) (map (\(l,r) -> l) convertedAnswers) 
    return $ [ "{ " ++ connectAtoms " ; " (map (\(l, r) -> l ++ " : " ++ (connectAtoms ", " r) ) convertedAnswers) ++ " } == 1 :- " ++ (connectAtoms ", " req)  ]
             ++ (concat $ map (\(ls, r) -> [ l ++ " :- " ++ r ++ ", " ++ (connectAtoms ", " req) | l <- ls] ) additionalTerms)

connectAtoms connector atoms = foldl (\ a b -> a ++ connector ++ b) (head atoms) (tail atoms)

replaceWithNewFunc [] = return []
replaceWithNewFunc ( (l, r) : ls) = freshASPFunctionName >>= (\f -> (replaceWithNewFunc ls) 
                                                >>= (\rest -> return $ ((f ++ "(" ++ (connectAtoms ", " r) ++ ")", r) : rest) ))

testEval a = evalState a $ ASPVarState 0 []
onlyOneTest = testEval $ onlyOne [ ([makeASPRule ["res", "A"], makeASPRule ["res2", "A"]], [makeASPRule ["req", "A"], makeASPRule ["req2", "A"]]) ] [makeASPRule ["typeVar", "A"]]

main =  putStrLn $ (connectAtoms ".\n" onlyOneTest )++ ".\n"

makeASPRule params = (head params) ++ "(" ++ (
    foldl (\ a b -> a ++ "," ++ b) (head . tail $ params) (tail . tail $ params)
    ) ++ ")"


data ASPVarState = ASPVarState { tphNum :: Int, other :: [ASPStatement]}

type ASPVar = String

type ASPVarStateMonad  = State ASPVarState
freshASPVar ::   ASPVarStateMonad  ASPVar
freshASPVar  = state  (\st -> let st' = ASPVarState ((tphNum st) +1) (other st) in ("V" ++ (show $ tphNum st'),st') )
freshASPFunctionName ::   ASPVarStateMonad  ASPVar
freshASPFunctionName  = state  (\st -> let st' = ASPVarState ((tphNum st) +1) (other st) in ( "fun" ++ (show $ tphNum st'),st') )
