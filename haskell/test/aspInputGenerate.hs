{-# LANGUAGE FlexibleContexts #-}
import Text.ParserCombinators.Parsec
import ASPGenerator

data JavaType = SimpleType String | ParamType String [JavaType] | ExtendsType JavaType deriving Show
type Constraint = (JavaType, Operator, JavaType)
data Operator = EqualsDotOp | SmallerOp | SmallerDotOp deriving Show

typeChars = (noneOf "?<=.>")

line = 
    do result <- constraint
       eof
       return result

parseString [] = return []
parseString s = let rest = parseString (tail s) in
    char (head s) >> rest

constraint :: GenParser Char st Constraint
constraint = do
    type1 <- typeDecl
    op <- operator
    type2 <- typeDecl
    return (type1, op, type2)

operator :: GenParser Char st Operator
operator = ((char '=') >> (char '.') >> (return EqualsDotOp))
    <|> ((char '<') >> (char '.') >> (return SmallerDotOp))
    <|> ((char '<') >> (return SmallerOp))

typeDecl :: GenParser Char st JavaType
typeDecl = do
    parseString "?extends"
    extendsType <- typeDecl 
    return $ ExtendsType extendsType
    <|>
    do  result <- many typeChars
        parameter <- params
        case parameter of
            [] -> return $ SimpleType result
            ps -> return $ ParamType result ps
       
params :: GenParser Char st [JavaType]
params = 
    do  char '<'
        first <- typeDecl
        next <- remainingParams
        char '>'
        return (first : next)
    <|> return []

remainingParams :: GenParser Char st [JavaType]
remainingParams =
    (char ',' >> params)          
    <|> (return [])                

paramType :: GenParser Char st String
paramType = 
    many (noneOf ",\n")

-- The end of line character is \n
eol :: GenParser Char st Char
eol = char '\n'

parseConstraint input = errorHandling $ parse line "(unknown)" (filter ((/=) ' ') input)
    where
        errorHandling (Left _) = error $ "Parser Error bei " ++ input
        errorHandling (Right s) = s

consToASP (t1, EqualsDotOp, t2) = do
    t1Var <- freshASPVar
    t2Var <- freshASPVar
    equalsDot <- return $ [EqualsDot t1Var t2Var]
    asp1 <- typeToASP t1Var t1
    asp2 <- typeToASP t2Var t2
    return $ Implication [Facts (asp1 ++ asp2 ++ equalsDot)] []

typeToASP pointer (ExtendsType et) = do
    etASP <- typeToASP pointer et
    return $ etASP ++ [Extends pointer]

typeToASP pointer (SimpleType name) = return $ [Type pointer name "0"]
typeToASP pointer (ParamType name params) = do
    pointerList <- replaceWithPointer params
    pointerParamNumList <- return $ zip pointerList [1 .. (length params)]
    aspParamList <- return $ map generateParam pointerParamNumList
    paramPointerList <- return $ zip params pointerList
    paramTypeList <- mapToParamList paramPointerList
    return $ [Type pointer name (show $ length params)] ++ (paramTypeList) ++ aspParamList
    where
        replaceWithPointer [] = return []
        replaceWithPointer ( l : ls) = freshASPVar >>= (\f -> (replaceWithPointer  ls) 
                                                >>= (\rest -> return $ (f : rest) ) )
        generateParam (tpointer, num) = Param pointer tpointer (show num)
        mapToParamList [] = return []
        mapToParamList ( (param,pointer) : ls) = (typeToASP pointer param) >>= (\t -> (mapToParamList ls) 
                                                >>= (\rest -> return $ (t ++ rest) ) )



main = putStrLn $ generateASPFromString ["List<Integer> =. ? extends Integer"]

generateASPFromString input = let cons = map parseConstraint input in
    concat $ map (generate . consToASP) cons