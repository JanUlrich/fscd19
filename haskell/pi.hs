import ASPGenerator

--pi(t1, t2, param1, param2) -> erstellt das mapping für die Parametertypen
--dabei ensteht param2, welcher dann der neue Parameter ist
-- Die pi Funktion ist im Paper nicht genau spezifiziert,
--  deren Korrektheit wird daher auch nicht bewiesen
piPermutation = do
    t1 <- freshASPVar
    t2 <- freshASPVar
    param1 <- freshASPVar
    param1Num <- freshASPVar
    param2Num <- freshASPVar
    param2 <- freshASPVar
    reqTypeVarOnlyT1 <- typeVarOnly t1
    reqTypeVarOnlyT2 <- typeVarOnly t2
    req <- return $ [ 
        TypeVar param1,
        Param t1 param1 param1Num ,
        Param t2 param1 param2Num]
        ++ reqTypeVarOnlyT1 ++ reqTypeVarOnlyT2
    res <- return $ [
        Pi t1 t2 param1Num param2Num ]
    return $ Implication [Facts res] req
    {-
        leftParamIsTPH = do
            param1 <- freshASPVar
            param1Num <- freshASPVar
            param2 <- freshASPVar
            req <- return $ [ 
                makeASPRule ["typeVar", param1],
                makeASPRule ["typeVar", param2],
                (param1 ++ " == " ++param2),
                makeASPRule ["param", t1, param1, param1Num] ]
            return $ "pi(" ++ ")"
    -}

typeVarOnly t = do
    i <- freshASPVar
    num <- freshASPVar
    param <- freshASPVar
    typeName <- freshASPVar
    req <- return $[
        Type t typeName num,
        Param t param i,
        TypeVar param,
        Function (i ++ " = 1 .. " ++ num) []
        ]
    return req

notWildcardASP = do
    a <- freshASPVar
    none <- return $ "_"
    reqS <- return $[
        Type a none none,
        Not $ Super a
        ]
    resS <- return $ [
        NotSuper a
        ]
    reqE <- return $[
        Type a none none,
        Not $ Extends a
        ]
    resE <- return $ [
        NotExtends a
        ]
    return $ Implication [
            Implication [Facts resS] reqS,
            Implication [Facts resE] reqE
            ] []

    -- "notSuper(A) :- not super(A), type(A, B,C).\nnotExtends(A) :- not extends(A), type(A, B,C)."


main = do
    writeFile "../asp/pi.pl" $ (generate piPermutation) ++ "\n" ++ (generate notWildcardASP)
    return ()