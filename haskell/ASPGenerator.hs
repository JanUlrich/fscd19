module ASPGenerator  
( makeASPRule
, connectAtoms
, freshASPVar
, freshASPFunctionName
, generate
, genPointer
, ASPStatement(..)
, SubImplication(..)
, ASPVarState(..)
, Value(..)
, ASPVar
, ASPVarStateMonad
) where  

import Control.Monad.State

data ASPStatement = Implication [ASPStatement] [Value] | Cart [SubImplication] | Facts [Value]
data SubImplication = CartImplication [ASPStatement] [Value]

data Value = EqualsDot ASPVar ASPVar 
          | SmallerDot ASPVar ASPVar
          | SmallerDotWC ASPVar ASPVar
          | Type ASPVar ASPVar ASPVar
          | Param ASPVar ASPVar ASPVar
          | TypeFC ASPVar ASPVar ASPVar
          | ParamFC ASPVar ASPVar ASPVar
          | TypeVar ASPVar
          | Smaller ASPVar ASPVar
          | GreaterGen ASPVar ASPVar
          | SmallerGen ASPVar ASPVar
          | Pi ASPVar ASPVar ASPVar ASPVar
          | Extends ASPVar
          | Super ASPVar
          | NotExtends ASPVar
          | NotSuper ASPVar
          | Function ASPVar [Value]
          | Not Value

instance Show Value where
    show (EqualsDot a b) = makeASPRule ["equals", a, b]
    show (SmallerDot a b) = makeASPRule["smallerDot", a, b]
    show (SmallerDotWC a b) = makeASPRule["smallerDotWC", a, b]
    show (Type a b c) = makeASPRule["type", a, b, c]
    show (Param a b c) = makeASPRule["param", a, b, c]
    show (TypeFC a b c) = makeASPRule["type", a, b, c]
    show (ParamFC a b c) = makeASPRule["param", a, b, c]
    show (TypeVar a) = makeASPRule["typeVar", a]
    show (Smaller a b) = makeASPRule["smaller", a, b]
    show (GreaterGen a b) = makeASPRule["greaterGen", a, b]
    show (SmallerGen a b) = makeASPRule["smallerGen", a, b]
    show (Pi a b c d) = makeASPRule["pi", a, b, c, d]
    show (Extends a) = makeASPRule["extendsWildcard", a]
    show (Super a) = makeASPRule["superWildcard", a]
    show (NotExtends a) = makeASPRule["notExtends", a]
    show (NotSuper a) = makeASPRule["notSuper", a]
    show (Function a (b:bs)) = makeASPRule $ (a : (map show (b:bs)))
    show (Function a []) = a
    show (Not a) = "not " ++ show a


data ASPVarState = ASPVarState { tphNum :: Int, other :: [ASPStatement]}

type ASPVar = String

type ASPVarStateMonad  = State ASPVarState
freshASPVar ::   ASPVarStateMonad  ASPVar
freshASPVar  = state  (\st -> let st' = ASPVarState ((tphNum st) +1) (other st) in ("V" ++ (show $ tphNum st'),st') )
freshASPFunctionName ::   ASPVarStateMonad  ASPVar
freshASPFunctionName  = state  (\st -> let st' = ASPVarState ((tphNum st) +1) (other st) in ( "fun" ++ (show $ tphNum st'),st') )
    

-- (X) statement
-- { funcN1(req1Vars, reqVars) : req1 ; funcN2(...) : req2 ; ... } == 1 :- req
-- 
generateASP req (Cart implications) = do
    --erg <- onlyOne implications req --(map (\(CartImplication as bs) -> (as, bs)) implications)
    replacedFunctions <- replaceWithNewFunc req implications
    onlyFunctions <- return $ (fst . unzip) replacedFunctions
    rest <- generateBodyListAdd req $ (map (\(CartImplication as bs, replacedFunc) -> (as, req ++ bs ++ [replacedFunc])) (zip implications onlyFunctions))
    erg <- return $ "{ " ++ connectStrings " ; " (map (\(l, r) -> show l ++ " : " ++ (connectAtoms ", " r) ) replacedFunctions) ++ " } == 1 :- " ++ (connectAtoms ", " req) ++ ".\n"
    return $ erg ++ rest
    where
        generateBodyListAdd req [] = return ""
        generateBodyListAdd req ((stmts, addReq) : ls) = (generateBodyList (req ++ addReq) stmts) >>= (\asp -> (generateBodyListAdd req ls) 
            >>= (\rest -> return (asp ++ rest) ))
        generateBodyList req [] = return ""
        generateBodyList req ( stmt : ls) = (generateASP req stmt) >>= (\asp -> (generateBodyList req ls) 
            >>= (\rest -> return (asp ++ rest) ))

-- Implication impls :- req
-- Falsch:
-- funcN(reqVars) :- req
-- impl1 :- funcN(reqVars)
-- impl2 :- funcN(reqVars)
-- ...
-- Es geht einfacher: einfach req an preReq anhängen:
generateASP preReq (Implication (impl:impls) reqImpl) = do
    req <- return $ preReq ++ reqImpl
    a <- generateASP req impl
    b <- generateASP preReq (Implication impls reqImpl)
    return $ a ++ b
generateASP preReq (Implication [] reqImpl) = do
    return $ ""

generateASP (req1:reqs) (Facts body) = do
    return $ let suffix = ( " :- " ++ connectAtoms ", " (req1:reqs) ++ ".\n") in
        connectAtoms suffix body ++ suffix
generateASP [] (Facts body) = do
    return $ connectAtoms ".\n" body ++ ".\n"

replaceWithNewFunc req [] = return []
replaceWithNewFunc req ( (CartImplication _ r) : ls) = freshASPFunctionName >>= (\f -> (replaceWithNewFunc req ls) 
                                                >>= (\rest -> return $ ((Function f (r ++ req)), r) : rest) )

generate aspMonad = evalState ( aspMonad >>= (\g -> generateASP [] g) ) $ ASPVarState 0 []

connectAtoms :: [Char] -> [Value] -> [Char]
connectAtoms connector atoms = connectStrings connector (map show atoms)

connectStrings connector (atom:atoms) = foldl (\ a b -> a ++ connector ++ b) (atom) (atoms)
connectStrings connector [] = []

makeASPRule (name:param1:params) = (name) ++ "(" ++ (
    foldl (\ a b -> a ++ "," ++ b) (param1) (params)
    ) ++ ")"

genPointer req = "pointer(" ++ (connectAtoms ", " req) ++ ")"