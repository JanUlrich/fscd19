import ASPGenerator

main = do
    writeFile "../asp/reduce.pl" $ generate reduce1 ++ generate reduceEq
    return ()

reduceEq = do
    c <- freshASPVar
    d <- freshASPVar
    name <- freshASPVar
    cParam <- freshASPVar
    dParam <- freshASPVar
    n <- freshASPVar
    paramNum <- freshASPVar
    req <- return $ [
        SmallerDot c d,
        Type c name n,
        Type d name n,
        Param c cParam paramNum,
        Param d dParam paramNum
        ]
    ret <- return $ [
        EqualsDot cParam dParam
        ]
    return $ Implication [Facts ret] req
    
reduce1 = do
    c <- freshASPVar
    d <- freshASPVar
    cFC <- freshASPVar
    dFC <- freshASPVar
    cName <- freshASPVar
    dName <- freshASPVar
    cParam <- freshASPVar
    dParam <- freshASPVar
    n <- freshASPVar
    paramNum1 <- freshASPVar
    paramNum2 <- freshASPVar
    req <- return $ [
        SmallerDot c d,
        Type c cName n,
        Type d dName n,
        Param c cParam paramNum1,
        Param d dParam paramNum2,
        TypeFC cFC cName n,
        TypeFC dFC dName n,
        Pi cFC dFC paramNum1 paramNum2
        ]
    ret <- return $ [
        SmallerDotWC cParam dParam
        ]
    return $ Implication [Facts ret] req