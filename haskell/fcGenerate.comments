import Control.Monad.State

type Atom = String
data ASPStatement = Implication [ASPStatement] [Atom] | Cart [SubImplication] | Facts [Atom]
data SubImplication = CartImplication [ASPStatement] [Atom]
{-

-}
generateFunction fromReqs = undefined

-- (X) statement
-- { funcN1(req1Vars, reqVars) : req1 ; funcN2(...) : req2 ; ... } == 1 :- req
-- 
generateASP req (Cart implications) = do
    --erg <- onlyOne implications req --(map (\(CartImplication as bs) -> (as, bs)) implications)
    replacedFunctions <- replaceWithNewFunc req implications
    onlyFunctions <- return $ (fst . unzip) replacedFunctions
    rest <- generateBodyListAdd req $ (map (\(CartImplication as bs, replacedFunc) -> (as, req ++ bs ++ [replacedFunc])) (zip implications onlyFunctions))
    erg <- return $ "{ " ++ connectAtoms " ; " (map (\(l, r) -> l ++ " : " ++ (connectAtoms ", " r) ) replacedFunctions) ++ " } == 1 :- " ++ (connectAtoms ", " req) ++ ".\n"
    return $ erg ++ rest
    where
        generateBodyListAdd req [] = return ""
        generateBodyListAdd req ((stmts, addReq) : ls) = (generateBodyList (req ++ addReq) stmts) >>= (\asp -> (generateBodyListAdd req ls) 
            >>= (\rest -> return (asp ++ rest) ))
        generateBodyList req [] = return ""
        generateBodyList req ( stmt : ls) = (generateASP req stmt) >>= (\asp -> (generateBodyList req ls) 
            >>= (\rest -> return (asp ++ rest) ))

-- Implication impls :- req
-- Falsch:
-- funcN(reqVars) :- req
-- impl1 :- funcN(reqVars)
-- impl2 :- funcN(reqVars)
-- ...
-- Es geht einfacher: einfach req an preReq anhängen:
generateASP preReq (Implication (impl:impls) reqImpl) = do
    req <- return $ preReq ++ reqImpl
    a <- generateASP req impl
    b <- generateASP preReq (Implication impls reqImpl)
    return $ a ++ b
generateASP preReq (Implication [] reqImpl) = do
    return $ ""

generateASP (req1:reqs) (Facts body) = do
    return $ let suffix = ( " :- " ++ connectAtoms ", " (req1:reqs) ++ ".\n") in
        connectAtoms suffix body ++ suffix
generateASP [] (Facts body) = do
    return $ connectAtoms ".\n" body ++ ".\n"

generateASPTest = evalState ( greater >>= (\g -> generateASP [] g) ) $ ASPVarState 0 []

{-
onlyOne erhält: [([lösungsMenge1, lösungsMenge2], Bedingungen), ...] :- [ req1, req2 ]
soll werden:
{ func1(Bedingungen) : Bedingungen; func2(Bedingungen) : Bedingungen ... } == 1 :- req
lösungsMenge1 :- func1(Bedingunge)
lösungsMenge2 :- func2(Bedingunge)

TODO:
Kombinieren von mehreren onlyOne (Karthesischen Produkten)
(X)(Bedingung) { (X)(Unterbedingung) {lösungsmenge | unterbedingung} | Bedingung} 


-}
connectAtoms connector atoms = foldl (\ a b -> a ++ connector ++ b) (head atoms) (tail atoms)

replaceWithNewFunc req [] = return []
replaceWithNewFunc req ( (CartImplication _ r) : ls) = freshASPFunctionName >>= (\f -> (replaceWithNewFunc req ls) 
                                                >>= (\rest -> return $ ((f ++ "(" ++ (connectAtoms ", " (r ++ req)) ++ ")", r) : rest) ))


{-
implication -> ImplicationTarget* "<-" Atoms*
ImplicatoinTarget -> implication | cartesianProduct | Atom
cartesianProd -> Atom       [implication]
               (Bedingung)  
   (x)      { ergebnisse | bedingung1 }{ ergebnisse | bedingung2 }
(PreBedingung) 

cartesianProduct wird zu:
{ func(ergebnisse) : bedingung1 ; ... : bedingung2 } == 1 :- PreBedingung
  (siehe unten)

a € B === "B(a)" (Atom)
a € greater(A) === "greater(A, a)", zusätzlich muss ein funktionsaufruf greater(A) generiert werden
-}

data ASPVarState = ASPVarState { tphNum :: Int, other :: [ASPStatement]}

type ASPVar = String

type ASPVarStateMonad  = State ASPVarState
freshASPVar ::   ASPVarStateMonad  ASPVar
freshASPVar  = state  (\st -> let st' = ASPVarState ((tphNum st) +1) (other st) in ("V" ++ (show $ tphNum st'),st') )
freshASPFunctionName ::   ASPVarStateMonad  ASPVar
freshASPFunctionName  = state  (\st -> let st' = ASPVarState ((tphNum st) +1) (other st) in ( "fun" ++ (show $ tphNum st'),st') )

-- Idee:
--Das Haskell Programm könte einen Baum als Datenstruktur generieren
--nur wenn dieser Baum vollständig ist, dann ist der Algorithmus vollständig
--Baum ist leichter zu lesen als das ASP Programm (quasi eine Übersicht)
--Aus diesem Baum wird ASP Programm generiert
--
--data Type = TVar String | Type String [Type] | Extends Type | Super Type
--data Type = TVar | Type | Extends | Super
--data Type = TVar | Type | TypeParameter

--data ASPType = Type {pointer :: String, name :: String}


-- Pi transformation:
-- 
{-
pi :: ASPRule
pi = ASPRule ls rs
	where ls = undefined
		  rs = undefined
-}

--none = "_"

{-
Bemerkungen zu smaller und greater:
FC: Matrix < List<Integer>

Bei smaller:
smaller(List<Integer>) = {Matrix}

Bei greater:
greater(Matrix) = {List<Integer>}

-}

{-
input: vorbedingung, t1
t1 ist der greater Typ

-}
greater = do
    t1 <- freshASPVar
    t2 <- freshASPVar
    paramIncrementI1 <- freshASPVar
    paramIncrementP1 <- freshASPVar
    paramIncrementI2 <- freshASPVar
    paramIncrementP2 <- freshASPVar
    fct <- freshASPVar
    fctGreater <- freshASPVar
    fctGreaterName <- freshASPVar
    t1Name <- freshASPVar
    t1NumParams <- freshASPVar
    t2NumParams <- freshASPVar
    --(piRes, piReq) <- piPermutation fct fctGreater
    req <- return $ [ 
                makeASPRule ["greaterGen", t1, t2]
                ]
    reqSmaller <- return $ [ 
                makeASPRule ["type", t1, t1Name, t1NumParams] ,
                makeASPRule ["typeFC", fct, t1Name, t1NumParams] ,
                makeASPRule ["typeFC", fctGreater, fctGreaterName, t2NumParams] ,
                makeASPRule ["smaller", fct, fctGreater]
                ]
    resNewType <- return $ [
                makeASPRule ["type", t2, fctGreaterName, t2NumParams]
                --, makeASPRule ["pi", fct, fctGreater]
                ]
    --Die einfachste Variante ohne Wildcards:
    param1Num <- freshASPVar
    param2Num <- freshASPVar
    param <- freshASPVar
    reqParams <- return $ [ 
                makeASPRule ["param", t1, param, param1Num],
                makeASPRule ["pi", fct, fctGreater, param1Num, param2Num ] ]
    --Param ist TV:
    reqTypeVarParam <- return $ [ 
                makeASPRule ["typeVar", param] ] --dabb kann der Typ einfach 1 zu 1 übernommen werden...
    --Param ist Type:
    paramTypeName <- freshASPVar
    paramTypeParamNumber <- freshASPVar
    reqNormalParam <- return $ [
                makeASPRule ["notSuper", param],
                makeASPRule ["notExtends", param],
                makeASPRule ["type", param, paramTypeName, paramTypeParamNumber]]
    --TODO: Dieser Typpointer sollte bei generateASP generiert werden
    newTypePointer <- return $ "pointer(" ++ (connectAtoms ", " (req ++ reqNormalParam)) ++ ")"
    resNormalParamGreater <- return $ [
                makeASPRule["extends", newTypePointer],
                makeASPRule["param", t2, newTypePointer, param2Num],
                makeASPRule["greaterGen", t2, newTypePointer]
                ]
    resNormalParamSmaller <- return $ [
                makeASPRule["super", newTypePointer],
                makeASPRule["param", t2, newTypePointer, param2Num],
                makeASPRule["smallerGen", t2, newTypePointer]
                ]
    resParamStays <- return $ [  -- Der Parameter wird 1 zu 1 übernommen
                makeASPRule ["param", t2, param, param2Num] ]
    -- Parameter ist super Wilcard ? super ...            
    reqSuperParam <- return $ [
                makeASPRule ["super", param],
                makeASPRule ["notExtends", param],
                makeASPRule ["type", param, paramTypeName, paramTypeParamNumber]]
    -- Parameter ist extends Wilcard ? extends ...
    reqExtendsParam <- return $ [
                makeASPRule ["notSuper", param],
                makeASPRule ["extends", param],
                makeASPRule ["type", param, paramTypeName, paramTypeParamNumber]]

    -- TODO: Was passiert wenn in der FC Matrix < List<Integer> steht.
    paramRes <- return $ Cart [(CartImplication [(Facts resParamStays)] reqTypeVarParam)
                        ,(CartImplication [(Facts resParamStays)] reqNormalParam)
                        ,(CartImplication [(Facts resNormalParamSmaller)] reqNormalParam)
                        ,(CartImplication [(Facts resNormalParamSmaller)] reqExtendsParam)
                        ,(CartImplication [(Facts resNormalParamGreater)] reqNormalParam)
                        ,(CartImplication [(Facts resNormalParamGreater)] reqSuperParam)
                        ]
    return $ Implication [
            ( Implication [ Cart [ CartImplication [(Implication [paramRes] reqParams), (Facts resNewType)] reqSmaller] ] req )
            , (Implication [(Facts ["notSuper(A)"])] ["not super(A)", "type(A, B,C)"])
            , (Implication [(Facts ["notExtends(A)"])] ["not extends(A)", "type(A, B,C)"])
            ] []
{-                    return $ (concat $ map (\(ls, r) -> [ l ++ " :- " ++ connectAtoms ", " r | l <- ls]) [(piRes, piReq ++ req), (res, req)])
             ++ paramRes --Es gibt mehrer Möglichkeiten bei den Parametern, es muss jeweils eine ausgewählt werden
             ++ ["notSuper(X) :- type(X, _,_), not super(X)", "notExtends(X) :- type(X, _,_), not extends(X)"]
-}
{-
Das wurde benutzt um sicherzugehen, dass fct und fctGreater nur TVs als Parameter haben

Dieser Check ist aber beispielsweise bei der reduce1 Regel notwendig.
reqNotInPointer <- return $ [
        (paramIncrementI2 ++ " = 1 .. " ++ t1NumParams),
        makeASPRule ["param", fct, paramIncrementP1, paramIncrementI1],
        makeASPRule ["typeVar", paramIncrementP1],
        (paramIncrementI2 ++ " = 1 .. " ++ t2NumParams),
        makeASPRule ["param", fctGreater, paramIncrementP2, paramIncrementI2],
        makeASPRule ["typeVar", paramIncrementP2]]
-}

--main = putStrLn $ connectAtoms ".\n" greaterTest ++ "."
main = do
    writeFile "../asp/greater.pl" generateASPTest
    return ()
--greaterTest = evalState (greater) $ ASPVarState 0 []

testEval a = evalState a $ ASPVarState 0 []


{-
{ paramGreater, paramSame } == 1 :- res, param
-}

{-
Ansatz 2:
Greater sollte als Baumstruktur ausgegeben werden, dieser Baum wird anschließend in ASP umgewandelt

-}

{-
Anmerkungen:
'where there are P1 - PN' aus dem Paper lässt sich in ASP Variablen übersetzen
Die Adapt Regel könnte auch einfach mit Haskell umgesetzt werden.
Haskell hilft aus, wenn sehr viele Sachen impliziert werden
-}


{-
--pi(t1, t2, param1, param2) -> erstellt das mapping für die Parametertypen
--dabei ensteht param2, welcher dann der neue Parameter ist
-- Die pi Funktion ist im Paper nicht genau spezifiziert,
--  deren Korrektheit wird daher auch nicht bewiesen
Bedingung:
Im Paper kommt die pi Funktion nur bei reduce vor und dort werden nur TPHs eingesetzt
-> Daher muss die Pi Funktion nur auf A<a1, a2> < B<b1 b2> € FC funktionieren,
	bei denen aN und bN alles TPHs sind
-}
piPermutation t1 t2 = do
    part1 <- bothParamsAreTPH
    return part1
    where
        bothParamsAreTPH = do
            param <- freshASPVar
            param1Num <- freshASPVar
            param2Num <- freshASPVar
            param2 <- freshASPVar
            req <- return $ [ 
                --makeASPRule ["typeVar", param1], -- diese sind überflüssig, da t1 und t2 die Bedingung (siehe oben) erfüllen müssen
                --makeASPRule ["typeVar", param2],
                makeASPRule ["param", t1, param, param1Num] ,
                makeASPRule ["param", t2, param, param2Num] ]
            res <- return $ [
                makeASPRule ["pi", t1, t2, param1Num, param2Num] ]
            return $ Implication [(Facts res)] req 
    {-
        leftParamIsTPH = do
            param1 <- freshASPVar
            param1Num <- freshASPVar
            param2 <- freshASPVar
            req <- return $ [ 
                makeASPRule ["typeVar", param1],
                makeASPRule ["typeVar", param2],
                (param1 ++ " == " ++param2),
                makeASPRule ["param", t1, param1, param1Num] ]
            return $ "pi(" ++ ")"
    -}

makeASPRule params = (head params) ++ "(" ++ (
    foldl (\ a b -> a ++ "," ++ b) (head . tail $ params) (tail . tail $ params)
    ) ++ ")"

piTest = evalState (piPermutation "test" "test2") $ ASPVarState 0 []
