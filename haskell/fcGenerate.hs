import ASPGenerator

{-
onlyOne erhält: [([lösungsMenge1, lösungsMenge2], Bedingungen), ...] :- [ req1, req2 ]
soll werden:
{ func1(Bedingungen) : Bedingungen; func2(Bedingungen) : Bedingungen ... } == 1 :- req
lösungsMenge1 :- func1(Bedingunge)
lösungsMenge2 :- func2(Bedingunge)

TODO:
Kombinieren von mehreren onlyOne (Karthesischen Produkten)
(X)(Bedingung) { (X)(Unterbedingung) {lösungsmenge | unterbedingung} | Bedingung} 


-}



{-
implication -> ImplicationTarget* "<-" Atoms*
ImplicatoinTarget -> implication | cartesianProduct | Atom
cartesianProd -> Atom       [implication]
               (Bedingung)  
   (x)      { ergebnisse | bedingung1 }{ ergebnisse | bedingung2 }
(PreBedingung) 

cartesianProduct wird zu:
{ func(ergebnisse) : bedingung1 ; ... : bedingung2 } == 1 :- PreBedingung
  (siehe unten)

a € B === "B(a)" (Atom)
a € greater(A) === "greater(A, a)", zusätzlich muss ein funktionsaufruf greater(A) generiert werden
-}

greater = do
    t1 <- freshASPVar
    t2 <- freshASPVar
    paramIncrementI1 <- freshASPVar
    paramIncrementP1 <- freshASPVar
    paramIncrementI2 <- freshASPVar
    paramIncrementP2 <- freshASPVar
    fct <- freshASPVar
    fctGreater <- freshASPVar
    fctGreaterName <- freshASPVar
    t1Name <- freshASPVar
    t1NumParams <- freshASPVar
    t2NumParams <- freshASPVar
    --(piRes, piReq) <- piPermutation fct fctGreater
    req <- return $ [ 
                GreaterGen t1 t2
                ]
    reqSmaller <- return $ [ 
                Type t1 t1Name t1NumParams ,
                TypeFC fct t1Name t1NumParams ,
                TypeFC fctGreater fctGreaterName t2NumParams ,
                Smaller fct fctGreater
                ]
    resNewType <- return $ [
                Type t2 fctGreaterName t2NumParams
                --, makeASPRule ["pi", fct, fctGreater]
                ]
    --Die einfachste Variante ohne Wildcards:
    param1Num <- freshASPVar
    param2Num <- freshASPVar
    param <- freshASPVar
    reqParams <- return $ [ 
                Param t1 param param1Num,
                Pi fct fctGreater param1Num param2Num ]
    --Param ist TV:
    reqTypeVarParam <- return $ [ 
                TypeVar param ] --dabb kann der Typ einfach 1 zu 1 übernommen werden...
    --Param ist Type:
    paramTypeName <- freshASPVar
    paramTypeParamNumber <- freshASPVar
    reqNormalParam <- return $ [
                NotSuper param,
                NotExtends param,
                Type param paramTypeName paramTypeParamNumber]
    --TODO: Dieser Typpointer sollte bei generateASP generiert werden
    newTypePointer <- return $ genPointer (req ++ reqNormalParam)
    resNormalParamGreater <- return $ [
                Extends newTypePointer,
                Param t2 newTypePointer param2Num,
                GreaterGen param newTypePointer
                ]
    resNormalParamSmaller <- return $ [
                Super newTypePointer,
                Param t2 newTypePointer param2Num,
                SmallerGen param newTypePointer
                ]
    resParamStays <- return $ [  -- Der Parameter wird 1 zu 1 übernommen
                Param t2 param param2Num ]
    -- Parameter ist super Wilcard ? super ...            
    reqSuperParam <- return $ [
                Super param,
                NotExtends param,
                Type param paramTypeName paramTypeParamNumber]
    -- Parameter ist extends Wilcard ? extends ...
    reqExtendsParam <- return $ [
                NotSuper param,
                Extends param,
                Type param paramTypeName paramTypeParamNumber]

    -- TODO: Was passiert wenn in der FC Matrix < List<Integer> steht.
    paramRes <- return $ Cart [(CartImplication [(Facts resParamStays)] reqTypeVarParam)
                        ,(CartImplication [(Facts resParamStays)] reqNormalParam)
                        ,(CartImplication [(Facts resNormalParamSmaller)] reqNormalParam)
                        ,(CartImplication [(Facts resNormalParamSmaller)] reqExtendsParam)
                        ,(CartImplication [(Facts resNormalParamGreater)] reqNormalParam)
                        ,(CartImplication [(Facts resNormalParamGreater)] reqSuperParam)
                        ]
    return $  Implication [ Cart [ CartImplication [(Implication [paramRes] reqParams), (Facts resNewType)] reqSmaller] ] req 
            
{-                    return $ (concat $ map (\(ls, r) -> [ l ++ " :- " ++ connectAtoms ", " r | l <- ls]) [(piRes, piReq ++ req), (res, req)])
             ++ paramRes --Es gibt mehrer Möglichkeiten bei den Parametern, es muss jeweils eine ausgewählt werden
             ++ ["notSuper(X) :- type(X, _,_), not super(X)", "notExtends(X) :- type(X, _,_), not extends(X)"]
-}

smaller = do
    t1 <- freshASPVar
    t2 <- freshASPVar
    paramIncrementI1 <- freshASPVar
    paramIncrementP1 <- freshASPVar
    paramIncrementI2 <- freshASPVar
    paramIncrementP2 <- freshASPVar
    fct <- freshASPVar
    fctSmaller <- freshASPVar
    fctSmallerName <- freshASPVar
    t1Name <- freshASPVar
    t1NumParams <- freshASPVar
    t2NumParams <- freshASPVar
    --(piRes, piReq) <- piPermutation fct fctGreater
    req <- return $ [ 
                SmallerGen t1 t2
                ]
    reqSmaller <- return $ [ 
                Type t1 t1Name t1NumParams ,
                TypeFC fct t1Name t1NumParams ,
                TypeFC fctSmaller fctSmallerName t2NumParams ,
                Smaller fctSmaller fct
                ]
    resNewType <- return $ [
                Type t2 fctSmallerName t2NumParams
                --, makeASPRule ["pi", fct, fctGreater]
                ]
    --Die einfachste Variante ohne Wildcards:
    param1Num <- freshASPVar
    param2Num <- freshASPVar
    param <- freshASPVar
    reqParams <- return $ [ 
                Param t1 param param1Num,
                Pi fctSmaller fct param2Num param1Num ]
    --Param ist TV:
    reqTypeVarParam <- return $ [ 
                TypeVar param ] --dabb kann der Typ einfach 1 zu 1 übernommen werden...
    --Param ist Type:
    paramTypeName <- freshASPVar
    paramTypeParamNumber <- freshASPVar
    reqNormalParam <- return $ [
                NotSuper param,
                NotExtends param,
                Type param paramTypeName paramTypeParamNumber]
    --TODO: Dieser Typpointer sollte bei generateASP generiert werden
    newTypePointer <- return $ genPointer (req ++ reqNormalParam)
    resNormalParamGreater <- return $ [
                Super newTypePointer,
                Param t2 newTypePointer param2Num
                ,GreaterGen param newTypePointer
                ]
    resNormalParamGreaterWithoutSuper <- return $ [
                Param t2 newTypePointer param2Num,
                GreaterGen param newTypePointer
                ]
    resNormalParamSmaller <- return $ [
                Extends newTypePointer,
                Param t2 newTypePointer param2Num,
                SmallerGen param newTypePointer
                ]
    resNormalParamSmallerWithoutExtends <- return $ [
            Param t2 newTypePointer param2Num,
            SmallerGen param newTypePointer
            ]
    resParamStays <- return $ [  -- Der Parameter wird 1 zu 1 übernommen
                Param t2 param param2Num ]
    -- Parameter ist super Wilcard ? super ...            
    reqSuperParam <- return $ [
                Super param,
                NotExtends param,
                Type param paramTypeName paramTypeParamNumber]
    -- Parameter ist extends Wilcard ? extends ...
    reqExtendsParam <- return $ [
                NotSuper param,
                Extends param,
                Type param paramTypeName paramTypeParamNumber]

    -- TODO: Was passiert wenn in der FC Matrix < List<Integer> steht.
    paramRes <- return $ Cart [(CartImplication [(Facts resParamStays)] reqTypeVarParam)
                        ,(CartImplication [(Facts resParamStays)] reqNormalParam)
                        ,(CartImplication [(Facts resNormalParamGreaterWithoutSuper)] reqSuperParam)
                        ,(CartImplication [(Facts resNormalParamGreater)] reqSuperParam)
                        ,(CartImplication [(Facts resNormalParamSmaller)] reqExtendsParam)
                        ,(CartImplication [(Facts resNormalParamSmallerWithoutExtends)] reqExtendsParam)
                        ]
    return $ Implication [Cart [ CartImplication [(Implication [paramRes] reqParams), (Facts resNewType)] reqSmaller]] req

smArg = do
    paramRes <- return $ Cart [(CartImplication [(Facts resParamStays)] reqTypeVarParam)
                        ,(CartImplication [(Facts resParamStays)] reqNormalParam)
                        ,(CartImplication [(Facts resNormalParamGreaterWithoutSuper)] reqSuperParam)
                        ,(CartImplication [(Facts resNormalParamGreater)] reqSuperParam)
                        ,(CartImplication [(Facts resNormalParamSmaller)] s)
                        ,(CartImplication [(Facts resNormalParamSmallerWithoutExtends)] reqExtendsParam)
                        ]
    return $ Implication [Cart [ CartImplication [(Implication [paramRes] reqParams), (Facts resNewType)] reqSmaller ++ reqExtendsParam]] reqSmallerArgGen


main = do
    writeFile "../asp/greater.pl" $ generate greater
    writeFile "../asp/smaller.pl" $ (generate smaller) -- ++ notWildcardASP
    return ()
--greaterTest = evalState (greater) $ ASPVarState 0 []


{-
{ paramGreater, paramSame } == 1 :- res, param
-}

{-
Ansatz 2:
Greater sollte als Baumstruktur ausgegeben werden, dieser Baum wird anschließend in ASP umgewandelt

-}

{-
Anmerkungen:
'where there are P1 - PN' aus dem Paper lässt sich in ASP Variablen übersetzen
Die Adapt Regel könnte auch einfach mit Haskell umgesetzt werden.
Haskell hilft aus, wenn sehr viele Sachen impliziert werden
-}

