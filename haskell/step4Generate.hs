import ASPGenerator

main = do 
    writeFile "../asp/step4.pl" $ (generate cart4) ++ (generate cart5)
    return ()

cart4 = do 
    a <- freshASPVar
    thetaPrime <- freshASPVar
    req <- return [
        SmallerDotWC a thetaPrime
        ]
    res <- return [
        EqualsDot a thetaPrime
        ]
    return $ Implication [Facts res] req

cart5 = do 
    a <- freshASPVar
    theta <- freshASPVar
    typeName <- freshASPVar
    typeNumParams <- freshASPVar
    req <- return [
        Smaller theta a
        ]
    thetaPrime <- return $ genPointer req
    subReq <- return [
        Type thetaPrime typeName typeNumParams
        ]
    preRes <- return [
        GreaterGen theta thetaPrime
        ]
    res <- return [
        EqualsDot a thetaPrime
        ]
    return $ Implication[
        Facts preRes
        , Cart [CartImplication [Facts res] subReq]
        ] req
    
{-


cart1 = do 
    unifyThetaOL <- freshASPVar
    theta <- freshASPVar
    thetaPrime <- freshASPVar
    thetaOL <- freshASPVar
    thetaOLPrime <- freshASPVar
    generateSets <- return [
        makeASPRule ["unify" unifyThetaOL thetaOLPrime thetaPrime]
    ]


-}
   {-

%reduce:
unify(Pointer, Param1, Param2)
 :- unify(Pointer, T1, T2), type(T1, TName, Num), type(T2, TName, Num)
        , param(T1, Param1, PNum), param(T2, Param2, PNum)
        .

%Ich lasse das Subst hier aus. Vector Beispiel funktioniert womöglich auch ohne

%swap:
unify(Pointer, B, A) :- unify(Pointer, A, B).

%Subst neu implementieren.
%Subst muss von hinten nach vorne durchgehen.
%Mit occurs kann man prüfen, welche unify(P, A, B) ausgetauscht werden müssen

%Diese Fälle dürfen nicht auftreten:
%TODO (möglicherweise braucht man sie nicht; Die Unifikation geht hier nie schief)

%Occurs Regeln:
occurs(A, A) :- typeVar(A).
occurs(A, T) :- type(T, _,_), param(T, B, _), occurs(A, B).

    -}
